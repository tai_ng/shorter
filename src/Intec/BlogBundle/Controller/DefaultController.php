<?php

namespace Intec\BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
    	$blogItem1 = new \stdClass;
    	$blogItem1->id = 1;
    	$blogItem1->title = "Title 1";
    	$blogItem1->subTitle = "Sub-Title 1";
    	$blogItem1->author = "Johan";
    	$blogItem1->postedON = "2015-02-01";

    	$blogItem2 = new \stdClass;
    	$blogItem2->id = 2;
    	$blogItem2->title = "Title 2";
    	$blogItem2->subTitle = "Sub-Title 2";
    	$blogItem2->author = "Dirk";
    	$blogItem2->postedON = "2010-12-04";

    	$blogItem3 = new \stdClass;
    	$blogItem3->id = 3;
    	$blogItem3->title = "Title 3";
    	$blogItem3->subTitle = "Sub-Title 3";
    	$blogItem3->author = "Frank";
    	$blogItem3->postedON = "2011-05-30";

    	$blogs = array($blogItem1, $blogItem2, $blogItem3);

        return $this->render('IntecBlogBundle:Default:index.html.twig', array('blogs' => $blogs));
    }

    public function itemAction($id, Request $request)
    {
    	$blogItem1 = new \stdClass;
    	$blogItem1->id = 1;
    	$blogItem1->title = "Title 1";
    	$blogItem1->subTitle = "Sub-Title 1";
    	$blogItem1->author = "Johan";
    	$blogItem1->postedON = "2015-02-01";

    	$blogItem2 = new \stdClass;
    	$blogItem2->id = 2;
    	$blogItem2->title = "Title 2";
    	$blogItem2->subTitle = "Sub-Title 2";
    	$blogItem2->author = "Dirk";
    	$blogItem2->postedON = "2010-12-04";

    	$blogItem3 = new \stdClass;
    	$blogItem3->id = 3;
    	$blogItem3->title = "Title 3";
    	$blogItem3->subTitle = "Sub-Title 3";
    	$blogItem3->author = "Frank";
    	$blogItem3->postedON = "2011-05-30";

    	$blogs = array($blogItem1, $blogItem2, $blogItem3);

    	foreach ($blogs as $value) {
    		if ($value->id == $id) {
    			$blog = $value;
    		}
    		if (!isset($blog)) {
    			$request->getSession()->getFlashBag()->add(
            		'notice',
            		'No such blog found'
        		);
    			return $this->redirect($this->generateUrl('intec_blog_homepage'));
    		}
    	}
        return $this->render('IntecBlogBundle:Default:item.html.twig', array('blog' => $blog));
    }
}
