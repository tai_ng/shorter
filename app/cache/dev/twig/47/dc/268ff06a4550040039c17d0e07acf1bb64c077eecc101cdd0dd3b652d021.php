<?php

/* IntecBlogBundle:Form:short.html.twig */
class __TwigTemplate_47dc268ff06a4550040039c17d0e07acf1bb64c077eecc101cdd0dd3b652d021 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_escape_filter($this->env, (isset($context["url"]) ? $context["url"] : $this->getContext($context, "url")), "html", null, true);
    }

    public function getTemplateName()
    {
        return "IntecBlogBundle:Form:short.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
