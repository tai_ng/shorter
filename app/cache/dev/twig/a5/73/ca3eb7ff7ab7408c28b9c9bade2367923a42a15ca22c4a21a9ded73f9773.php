<?php

/* IntecBlogBundle:Default:item.html.twig */
class __TwigTemplate_a573ca3eb7ff7ab7408c28b9c9bade2367923a42a15ca22c4a21a9ded73f9773 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h1 style=\"color:violet \">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["blog"]) ? $context["blog"] : $this->getContext($context, "blog")), "title", array()), "html", null, true);
        echo "</h1>
<h2 style=\"color:burlywood \">";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["blog"]) ? $context["blog"] : $this->getContext($context, "blog")), "subTitle", array()), "html", null, true);
        echo "</h2>
<p><i>";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["blog"]) ? $context["blog"] : $this->getContext($context, "blog")), "author", array()), "html", null, true);
        echo "</i></p>
<p><i>";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["blog"]) ? $context["blog"] : $this->getContext($context, "blog")), "postedON", array()), "html", null, true);
        echo "</i></p>";
    }

    public function getTemplateName()
    {
        return "IntecBlogBundle:Default:item.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 4,  28 => 3,  24 => 2,  19 => 1,);
    }
}
